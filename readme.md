Exemplary setup

## Available scripts

### `npm run html-dev`

Runs development environment for html/css devs. They work only in html_css folder.
The key task is to style every component from bottom to top. For example, if there is
a header component which contains button component and logo component, the task is to
1) write markup and style button with separate scss file called button.scss
2) write markup and style logo with separate scss file called logo.scss
3) write markup and style whole header with separate scss file called header.scss

All those elements should just be rendered on the page one by one.

!!!SCSS files must be imported to main.scss using `@import "[name of scss file]";`

!!!index.html is the only file where all components will be rendered. That is why
every component markup should start with a clearly visible comment.

Automatic bundle and hot reload features are enabled. After running the command
output html will be opened in a browser and automatically reloaded on every change
in index.html or scss files, imported to main.scss.

### `npm run react-dev`

Runs development environment for react devs. They work only in react_app folder.
React guys get styling and markup for components from html_css folder and make
the app up and running. SCSS files become encapsulated in components through
CSS modules. All dummy data is stored in db.json file and becomes accessible
through AJAX requests using local json-server. When the API will be ready all those request will be
easily reconfigured.

## To exit from dev mode press ctrl+c in a terminal
