const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack');

module.exports = {
    mode: "development",

    entry: [
        './html_css/src/index.js'
    ],

    devtool: "source-maps",

    devServer: {
        contentBase: path.join(__dirname, 'html_css', 'src'),
        watchContentBase: true,
        hot: true,
        open: true,
        inline: true
    },


    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve('./html_css/src/index.html')
        }),
        new webpack.HotModuleReplacementPlugin()
    ],


    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },

            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },

            {
                test: /\.(jpg|jpeg|gif|png|svg|webp)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            outputPath: './images',
                            name: "[name].[ext]",
                        },
                    },
                ]
            },

            {
                test: /\.html$/,
                use: {
                    loader: 'html-loader',
                }
            }
        ]
    }
}
