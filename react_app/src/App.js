import React, {Component} from 'react';
import axios from 'axios';

class App extends Component {

    state = {
        data: ''
    }


    componentDidMount() {
        axios.get('/posts')
            .then(data => {
                this.setState({
                    data: JSON.stringify(data.data)
                })
            })
    }


    render() {
        return (
            <div className="App">
                {this.state.data}
            </div>
        );
    }
}

export default App;
